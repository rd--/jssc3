export function encodeUsing(
	byteCount: number,
	writerFunction: (x: DataView) => void,
): Uint8Array {
	const arrayBuffer = new ArrayBuffer(byteCount);
	writerFunction(new DataView(arrayBuffer));
	return new Uint8Array(arrayBuffer);
}

export function encodeUint8(aNumber: number): Uint8Array {
	return encodeUsing(1, (b) => b.setUint8(0, aNumber));
}

export function encodeInt8(aNumber: number): Uint8Array {
	return encodeUsing(1, (b) => b.setInt8(0, aNumber));
}

export function encodeInt16(
	aNumber: number,
	littleEndian: boolean,
): Uint8Array {
	return encodeUsing(2, (b) => b.setInt16(0, aNumber, littleEndian));
}

export function encodeInt32(
	aNumber: number,
	littleEndian: boolean,
): Uint8Array {
	return encodeUsing(4, (b) => b.setInt32(0, aNumber, littleEndian));
}

// encodeFloat32(1.0, false) //= [63, 128, 0, 0]
export function encodeFloat32(
	aNumber: number,
	littleEndian: boolean,
): Uint8Array {
	return encodeUsing(4, (b) => b.setFloat32(0, aNumber, littleEndian));
}

export function encodeFloat64(
	aNumber: number,
	littleEndian: boolean,
): Uint8Array {
	return encodeUsing(8, (b) => b.setFloat64(0, aNumber, littleEndian));
}

export function encodeTypedArray(
	inputArray: Float32Array | Float64Array,
	elementSize: number,
	writerFunction: (x: DataView, o: number, i: number) => void,
): Uint8Array {
	const arrayBuffer = new ArrayBuffer(inputArray.length * elementSize);
	const dataView = new DataView(arrayBuffer);
	for (let i = 0; i < inputArray.length; i++) {
		writerFunction(dataView, i * elementSize, inputArray[i]);
	}
	const uint8Array = new Uint8Array(arrayBuffer);
	return uint8Array;
}

export function encodeFloat32Array(
	inputArray: Float32Array,
	littleEndian: boolean,
): Uint8Array {
	return encodeTypedArray(
		inputArray,
		4,
		(v, o, i) => v.setFloat32(o, i, littleEndian),
	);
}

export function encodeFloat64Array(
	inputArray: Float64Array,
	littleEndian: boolean,
): Uint8Array {
	return encodeTypedArray(
		inputArray,
		8,
		(v, o, i) => v.setFloat64(o, i, littleEndian),
	);
}

// encodePascalString('string') //= [6, 115, 116, 114, 105, 110, 103]
export function encodePascalString(aString: string): Uint8Array {
	const uint8Array = new Uint8Array(aString.length + 1);
	uint8Array[0] = aString.length;
	for (let i = 1; i < aString.length + 1; i++) {
		uint8Array[i] = aString.charCodeAt(i - 1);
	}
	return uint8Array;
}
