# Fonts

- APL 333 & APL 385: <http://www.apl385.com/fonts/>
- Computer Modern: <https://github.com/vsalvino/computer-modern>
- Los Altos, Parc Place: <http://www.kreativekorp.com/software/fonts/urbanrenewal/>
- Monaco: <https://github.com/taodongl/monaco.ttf>
- Neo Euler: <https://github.com/aliftype/euler-otf>
- ProFont: <https://tobiasjung.name/profont/>
- Virtue: <http://www.scootergraphics.com/virtue/>

Ttf files can be converted to Woff2 files using
<https://github.com/google/woff2>.
